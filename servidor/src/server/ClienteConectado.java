package server;

import com.mygdx.game.servidor.dto.PlayerDataDTO;
import vero.encriptar.Encriptacion;


import java.io.*;
import java.net.Socket;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Observable;

public class ClienteConectado extends Observable implements Runnable {


    private ObjectInputStream entrada;
    private ObjectOutputStream salida;
    public Socket socketCliente;
    public Key clave;
    ArrayList<ClienteConectado> clienteConectados;


    public ClienteConectado(Socket socketCliente, ArrayList<ClienteConectado> clienteConectados) throws IOException {
        this.clienteConectados = clienteConectados;
        this.socketCliente = socketCliente;
        entrada = new ObjectInputStream(this.socketCliente.getInputStream());
        salida = new ObjectOutputStream(this.socketCliente.getOutputStream());
        try {
            clave = Encriptacion.generarClave();
        } catch (NoSuchAlgorithmException e) {
            setChanged();
            notifyObservers("Imposible generar clave");
        }
    }

    public void escribir(byte[] mensaje) throws Exception {
        salida.writeObject(Encriptacion.encriptar(mensaje, clave));
    }

    @Override
    public void run() {
        try {
            salida.writeObject(clave);
        } catch (IOException e) {
            setChanged();
            notifyObservers("Imposible mandar clave al cliente.");
        }

        while (socketCliente.isConnected()) {

            Object guardarMensajeRecibido = null;

            try {
                guardarMensajeRecibido = entrada.readObject();
                byte[] responsee = null;
                try {

                    responsee = Encriptacion.desencriptar((byte[]) guardarMensajeRecibido, clave);
                } catch (Exception e) {

                }

                Object serverResponse = null;
                //Convierte el array de bytes a un objeto (serializable)
                try {
                    ByteArrayInputStream bis = new ByteArrayInputStream(responsee);
                    ObjectInput in = null;
                    try {
                        in = new ObjectInputStream(bis);
                        serverResponse = in.readObject();
                    } finally {
                        try {
                            if (in != null) {
                                in.close();
                            }
                        } catch (IOException ex) {

                        }
                    }
                } catch (Exception e) {

                }

                setChanged();
                notifyObservers(serverResponse);

            } catch (EOFException e) {
                setChanged();
                notifyObservers("Cliente desconectado");
                clienteConectados.remove(this);
                return; // es lomismo que el break
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

    }


}
