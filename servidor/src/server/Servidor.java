package server;

import gui.Controlador;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Servidor implements Runnable {


    ArrayList<ClienteConectado> clienteConectados = new ArrayList<>();
    private ServerSocket socketServidor;
    Thread serverThread;
    public boolean seguir = true;

    private Controlador controlador;

    public void setObserver(Controlador controlador){
        this.controlador = controlador;
    }

    /**
     * Actualiza el log de la vista principal
     * @param text String mensaje que se va a mostrar
     */
    private synchronized void updateLog(String text){
        controlador.actualizarPantalla(text);
    }


    public synchronized void stopServer(){
        seguir = false;
    }
    public void startServer() throws IOException {
        socketServidor = new ServerSocket(9876);
        serverThread = new Thread(this);
        serverThread.start();
    }

    @Override
    public void run() {
        //Creo el socket del servidor
        try {
            // para que espere 1s a una conexion y despues vuelva a escuchar
            // (si tiene que seguir escuchando)
            socketServidor.setSoTimeout(1000);
        }catch (Exception e){

        }
        updateLog("Servidor: Escuchando");
        while (seguir) {
            //Creo el socket del cliente
            ClienteConectado clienteConectado;

            try{
                Socket sCliente = socketServidor.accept();
                updateLog("Servidor: Cliente conectado");
                //Creo un nuevo cliente
                clienteConectado = new ClienteConectado(sCliente, clienteConectados);

                clienteConectado.addObserver(controlador);
                Thread t = new Thread(clienteConectado);
                t.start();
            } catch (IOException e) {

            }
        }
        controlador.actualizarPantalla("Servidor parado");
    }
}
