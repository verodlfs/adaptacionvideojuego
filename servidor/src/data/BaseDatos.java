package data;

import data.dao.ScoreDAO;

import java.io.*;
import java.sql.*;
import java.util.Properties;

public class BaseDatos {
    private String ip;
    private String user;
    private String password;
    private Connection conexion;
    public static final BaseDatos context = new BaseDatos();


    //DAO
    public ScoreDAO score;


    private BaseDatos() {
        getPropValues();
        conectar();
        score = new ScoreDAO(conexion);
    }

    String getIP() {
        return ip;
    }
    String getUser() {
        return user;
    }
    String getPassword() {
        return password;
    }

    public boolean conectar(){


        try {
            conexion = DriverManager.getConnection(
                    "jdbc:mysql://"+ip+":3306/videojuego",user, password);
            return true;
        } catch (SQLException sqle) {
            try {
                conexion = DriverManager.getConnection(
                        "jdbc:mysql://"+ip+":3306/",user, password);

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

                return true;
            } catch (SQLException | IOException e) {
                e.printStackTrace();
                return false;
            }
        }

    }


    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("videojuegoJava.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
        }
    }

    /**
     * Parar el servidor
     */
    public void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");


        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

    }

    void setPropValues(String ip, String user, String pass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);

            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;

    }

}
