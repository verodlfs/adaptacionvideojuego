package data.dao;

import data.modelo.InformacionUsuario;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ScoreDAO {

    Connection conexion;

    public ScoreDAO(Connection conexion){
        this.conexion = conexion;
    }

    public void insertarInformacion(InformacionUsuario score) {
        String sentenciaSql = "INSERT INTO datos (nombre, puntuacion) VALUES (?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, score.nombreUsuario);
            sentencia.setInt(2, score.puntuacion);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    public void modificarInformacion(InformacionUsuario score){

        String sentenciaSql = "UPDATE datos SET nombre = ?, puntuacion = ? " +
                "WHERE idjugador = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, score.nombreUsuario);
            sentencia.setInt(2, score.puntuacion);
            sentencia.setInt(3, score.id);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }


    public void borrarInformacion(int idjugador) {
        String sentenciaSql = "DELETE FROM datos WHERE idjugador = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idjugador);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    public ResultSet consultarTodo() throws SQLException {
        String sentenciaSql = "SELECT concat(idjugador) as 'ID', concat(nombre) as 'nombre', concat(puntuacion) as 'puntuacion' FROM datos";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();

        return resultado;
    }

    public ArrayList<InformacionUsuario> obtenerTodoComoArrayList() throws SQLException {
        String sentenciaSql = "SELECT concat(idjugador) as 'ID', concat(nombre) as 'nombre', concat(puntuacion) as 'puntuacion' FROM datos";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        ArrayList<InformacionUsuario> scores = new ArrayList<>();
        while (resultado.next()){
            scores.add(new InformacionUsuario(resultado.getInt(1), resultado.getString(2), resultado.getInt(3)));
        }
        return scores;
    }
}
