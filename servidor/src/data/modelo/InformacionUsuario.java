package data.modelo;

import java.io.*;
import java.sql.*;
import java.util.Properties;

public class InformacionUsuario {

    public String nombreUsuario;
    public int puntuacion;
    public int id;

    public InformacionUsuario(int id, String nombreUsuario, int puntuacion) {
        this.id = id;
        this.nombreUsuario = nombreUsuario;
        this.puntuacion = puntuacion;
    }
}
