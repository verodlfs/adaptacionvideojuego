package gui;

import com.mygdx.game.servidor.dto.PlayerDataDTO;
import data.BaseDatos;

import data.modelo.InformacionUsuario;
import server.ClienteConectado;
import server.Servidor;
import vero.encriptar.Encriptacion;

import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.security.Key;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

public class Controlador implements WindowListener, ActionListener, Observer {

    private BaseDatos dbc;
    private Vista vista;
    private Servidor servidor;
    boolean refrescar;
    private Key clave;


    public Controlador(BaseDatos dbc, Vista vista, Servidor servidor){


        this.dbc = dbc;
        this.vista = vista;
        this.servidor = servidor;
        this.servidor.setObserver(this);

        addActionListeners(this);
        addItemListener(this);
        addWindowListener(this);

        refrescarInformacion();
    }

    @Override
    public void update(Observable o, Object mensajeDelServidor) {

        if(o instanceof ClienteConectado) {
           if(mensajeDelServidor instanceof PlayerDataDTO) {
               String text = ((PlayerDataDTO) mensajeDelServidor).getNombre();
               int puntos = ((PlayerDataDTO)mensajeDelServidor).getScore();
               //Para mandar la respuesta al cliente que ha enviado el mensaje
               ClienteConectado clienteConectado = (ClienteConectado) o;

               //Para añadir el mensaje a la BD
               dbc.score.insertarInformacion(new InformacionUsuario(0,((PlayerDataDTO)mensajeDelServidor).getNombre(),((PlayerDataDTO)mensajeDelServidor).getScore()));
               refrescarInformacion();
               actualizarPantalla("Cliente: " + text+" con puntuacion: "+ puntos);

           }else if(mensajeDelServidor instanceof String) {
               actualizarPantalla((String) mensajeDelServidor);
           }else if(mensajeDelServidor instanceof Exception){
               actualizarPantalla("Hay una excepcion en el cliente" + ((Exception) mensajeDelServidor).getMessage());
           }else{
               actualizarPantalla("Ha ocurrido un error : is type "+mensajeDelServidor.getClass());
           }
        }else{

        }


    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Salir":
                System.exit(0);
                break;
            case "Desconectar":
                dbc.desconectar();
                break;
            case "Anadir":
                dbc.score.insertarInformacion(new InformacionUsuario(0, vista.txtJugador.getText(), Integer.parseInt((String)vista.txtPuntuacion.getText())));
                refrescarInformacion();
                borrarCampos();
                break;
            case "Eliminar":
                dbc.score.borrarInformacion(Integer.parseInt((String)vista.tabla.getValueAt(vista.tabla.getSelectedRow(), 0)));
                borrarCampos();
                refrescarInformacion();
                break;
            case "Modificar":
                dbc.score.modificarInformacion(new InformacionUsuario(
                        Integer.parseInt((String)vista.tabla.getValueAt(vista.tabla.getSelectedRow(),0)),
                        (String) vista.tabla.getValueAt(vista.tabla.getSelectedRow(),1),
                        Integer.parseInt((String)vista.tabla.getValueAt(vista.tabla.getSelectedRow(),2))
                ));
                refrescarInformacion();
                borrarCampos();
                break;
            case "IniciarServidor":
                vista.btnPararServidor.setEnabled(true);
                vista.btnIniciarServidor.setEnabled(false);
                try {
                    servidor.startServer();
                } catch (IOException ex) {
                    actualizarPantalla(ex.getLocalizedMessage());
                    //ex.printStackTrace();
                }
                break;
            case "PararServidor":
                servidor.stopServer();
                vista.btnPararServidor.setEnabled(false);
                vista.btnIniciarServidor.setEnabled(true);
                break;
        }
    }

    public void actualizarPantalla(String mensaje){
        vista.log_servidor.append("\n"+mensaje);
    }

    private void addWindowListener(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    private void addActionListeners(ActionListener listener) {
        vista.itemDesconectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);

        vista.btnAnadir.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnModificar.addActionListener(listener);
        vista.btnIniciarServidor.addActionListener(listener);
        vista.btnPararServidor.addActionListener(listener);
    }

    private void addItemListener(Controlador controlador) {
    }

    private void refrescarInformacion() {
        actualizarInformacion();
        refrescar = false;
    }

    private void actualizarInformacion() {
        try {
            vista.tabla.setModel(construirTablaDatos(dbc.score.consultarTodo()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private TableModel construirTablaDatos(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtm.setDataVector(data, columnNames);

        return vista.dtm;
    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    private void borrarCampos(){
        vista.txtJugador.setText("");
        vista.txtPuntuacion.setText("");
    }


    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
