package gui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Vista extends JFrame {
    JPanel panel;
    JTextField txtJugador;
    JTextField txtPuntuacion;
    JButton btnAnadir;
    JButton btnEliminar;
    JButton btnModificar;
    JTable tabla;
    JButton btnIniciarServidor;
    JButton btnPararServidor;
    JTextArea log_servidor;
    final static String TITULOFRAME="Servidor Puntuaciones";

    DefaultTableModel dtm;

    JMenuItem itemDesconectar;
    JMenuItem itemSalir;

    public Vista(){
        init();
    }

    private void init() {
        JFrame frame = new JFrame(TITULOFRAME);
        this.setContentPane(panel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        setMenu();
        setTableModels();
    }

    private void setTableModels() {
        this.dtm = new DefaultTableModel();
        this.tabla.setModel(dtm);
    }

    private void setMenu() {
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public JTextField getTxtJugador() {
        return txtJugador;
    }

    public void setTxtJugador(JTextField txtJugador) {
        this.txtJugador = txtJugador;
    }

    public JTextField getTxtPuntuacion() {
        return txtPuntuacion;
    }

    public void setTxtPuntuacion(JTextField txtPuntuacion) {
        this.txtPuntuacion = txtPuntuacion;
    }

    public JButton getBtnAnadir() {
        return btnAnadir;
    }

    public void setBtnAnadir(JButton btnAnadir) {
        this.btnAnadir = btnAnadir;
    }

    public JButton getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(JButton btnEliminar) {
        this.btnEliminar = btnEliminar;
    }

    public JButton getBtnModificar() {
        return btnModificar;
    }

    public void setBtnModificar(JButton btnModificar) {
        this.btnModificar = btnModificar;
    }

    public JTable getTabla() {
        return tabla;
    }

    public void setTabla(JTable tabla) {
        this.tabla = tabla;
    }

    public static String getTITULOFRAME() {
        return TITULOFRAME;
    }

    public DefaultTableModel getDtm() {
        return dtm;
    }

    public void setDtm(DefaultTableModel dtm) {
        this.dtm = dtm;
    }

    public JMenuItem getItemDesconectar() {
        return itemDesconectar;
    }

    public void setItemDesconectar(JMenuItem itemDesconectar) {
        this.itemDesconectar = itemDesconectar;
    }

    public JMenuItem getItemSalir() {
        return itemSalir;
    }

    public void setItemSalir(JMenuItem itemSalir) {
        this.itemSalir = itemSalir;
    }
}
