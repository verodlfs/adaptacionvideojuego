package gui;

import data.BaseDatos;
import server.Servidor;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        BaseDatos dbc = BaseDatos.context;
        Vista vista= new Vista();
        Servidor servidor = new Servidor();
        Controlador controlador = new Controlador(dbc,vista, servidor);

    }
}
