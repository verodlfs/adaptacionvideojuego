package test;

import data.BaseDatos;
import data.dao.ScoreDAO;
import data.modelo.InformacionUsuario;
import org.junit.jupiter.api.Test;
import server.ClienteConectado;
import server.Servidor;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

public class TestServidor {

    //STOP

    /**
     * 1º Se inicia el servidor
     * 2º Se para el servidor
     * 3º Se comprueba que el servidor se ha parado
     */
    @Test
    void serverStopCorrectoTest(){

        Servidor servidor = new Servidor();
        try {
            servidor.startServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
        servidor.stopServer();
    //    boolean esperado = false;
      //  assertEquals(esperado, servidor.seguir);
        assertFalse(servidor.seguir);
    }

    @Test
    void serverStopIncorrectoTest(){

        Servidor servidor = new Servidor();
        try {
            servidor.startServer();
        } catch (IOException e) {
            System.out.println("El valor esperado era true");
        }
        servidor.stopServer();
       // boolean esperado = true;
       // assertEquals(esperado, servidor.seguir);
        assertTrue(servidor.seguir);
    }
//////////////////////////////////////////////
    //START
    @Test
    void serverStartCorrectoTest(){

        Servidor servidor = new Servidor();
        boolean esperado = true;
        try {
            servidor.startServer();
        } catch (IOException e) {
            System.out.println("El valor esperado era true");
            esperado = false;
        }

        assertTrue(esperado);
    }

    @Test
    void serverStartInCorrectoTest(){

        Servidor servidor = new Servidor();
        boolean esperado = true;
        try {
            servidor.startServer();
        } catch (IOException e) {
            System.out.println("El valor esperado era true");
            esperado = false;
        }


        assertFalse(esperado);
    }


///////////////////////////////

    @Test
    void desconectarBDIncorrectoTest(){
        BaseDatos bd = BaseDatos.context;
        boolean esperado = false;
        bd.desconectar();

        assertTrue(esperado);
    }

    @Test
    void desconectarBDCorrectoTest(){

        BaseDatos bd = BaseDatos.context;
        bd.desconectar();
        boolean esperado = false;

        assertFalse(esperado);
    }

/////////////////////////////////////////
   /* @Test
    void conectarBDIncorrectoTest(){

        BaseDatos bd = BaseDatos.context;
        bd.conectar();
        boolean esperado = false;

        assertTrue(esperado);
    }*/
//CONECTAR BD
    /**
     * El metodo conectar de base de datos, devuelve true o false dependiendo
     * del estado de la base de datos,
     * true --> cuando la conexion se ha realizado correctamente
     * false --> cuando no ha podido realizar la conexion
     * (Probar este test teniendo el mysql del xamp  arrancado: true(conectara)
     * (Si se prueba este test teniendo el mysql del xamp parado: false(no conectara)
     */
    @Test
    void conectarBDCorrectoTest(){

        BaseDatos bd = BaseDatos.context;
        assertTrue(bd.conectar());
    }

    @Test
    void conectarBDIncorrectoTest(){

        BaseDatos bd = BaseDatos.context;
        assertFalse(bd.conectar());
    }

    //CONSULTAS

    /**
     * En este test se analiza si hay datos en la base de datos, utilizando el metodo consultarTodo se devuelve un
     * objeto ResultSet,
     * Bibliografia: https://javarevisited.blogspot.com/2016/10/how-to-check-if-resultset-is-empty-in-Java-JDBC.html
     */
    @Test
    void comprobarBdNoVacia(){
        BaseDatos bd = BaseDatos.context;
        bd.conectar();
        try {
            assertTrue( bd.score.consultarTodo().next());
            //Otra forma
           // ResultSet rs = bd.score.consultarTodo();
           // assertTrue( rs.next());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


///////////////////////////////////////////////

/*    @Test
    void insertarInfoCorrectoTest() throws IOException {

        BaseDatos bd = BaseDatos.context;
        bd.conectar();
        bd.score.insertarInformacion((InformacionUsuario) conexion);
        boolean esperado = true;

        assertTrue(esperado);
    }

    @Test
    void insertarInfoIncorrectoTest() throws IOException {
        Connection conexion = null;
        BaseDatos bd = BaseDatos.context;
        bd.score.insertarInformacion((InformacionUsuario) conexion);
        boolean esperado = false;

        assertTrue(esperado);
    }*/
}
