package vero.encriptar;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import java.math.BigInteger;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Encriptacion {

    //Algoritmo a emplear AES:
    private final static String alg = "AES";

    // Generar una clave de 128 bits adecuada para AES
    public static byte[] encriptar(byte[] texto, Key clave) throws Exception {

        //Se obtiene un cifrador AES
        Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");

        //Se encripta el texto
        aes.init(Cipher.ENCRYPT_MODE, clave);
        byte[] encriptado = aes.doFinal(texto);

        return encriptado;
    }

    public static byte[] desencriptar(byte[] encriptado, Key clave) throws Exception {
        Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");

        // Se encripta el texto
        aes.init(Cipher.ENCRYPT_MODE, clave);

        // Se inicia el cifrador para desencriptar, con la misma clave y se desencripta
        aes.init(Cipher.DECRYPT_MODE, clave);
        byte[] desencriptado = aes.doFinal(encriptado);
        return  desencriptado;
    }

    public static Key generarClave() throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(128);
        return keyGenerator.generateKey();
    }

    public static String obtenerHash(String texto) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");

        //Se cifra a bytes
        byte[] messageDigest = md.digest(texto.getBytes());
        BigInteger number = new BigInteger(1, messageDigest);

        //Se convierte a String el texto cifrado
        String hashtext = number.toString(16);
        while (hashtext.length() < 32) {
            hashtext = "0" + hashtext;
        }

        return hashtext;
    }

    public static boolean verificarHAsh(String contenido, String hash) throws Exception {
        return obtenerHash(contenido).equals(hash);
    }

    }
