package vero.datos;

import java.io.Serializable;

public class UsuarioDTO implements Serializable {

    private String nombreDeUsuario;
    private int puntuacion;


    public UsuarioDTO(String nombreDeUsuario, int puntuacion) {
        this.nombreDeUsuario = nombreDeUsuario;
        this.puntuacion = puntuacion;
    }

    public String getNombreDeUsuario() {
        return nombreDeUsuario;
    }

    public void setNombreDeUsuario(String nombreDeUsuario) {
        this.nombreDeUsuario = nombreDeUsuario;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }
}
