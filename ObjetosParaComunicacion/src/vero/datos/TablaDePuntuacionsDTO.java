package vero.datos;

import java.io.Serializable;
import java.util.List;

/**
 * Este objeto esta diseñado para que se pueda mandar la tabla de puntuaciones al cliente (el vidiojuego).
 */
public class TablaDePuntuacionsDTO implements Serializable {

    private List<UsuarioDTO> Usuarios;

    public TablaDePuntuacionsDTO(List<UsuarioDTO> usuarios) {
        Usuarios = usuarios;
    }

    public List<UsuarioDTO> getUsuarios() {
        return Usuarios;
    }

    public void setUsuarios(List<UsuarioDTO> usuarios) {
        Usuarios = usuarios;
    }
}
